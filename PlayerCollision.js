﻿var matchGUI : GameObject;
private var haveMatches : boolean = false;

var batteryCollect : AudioClip;
private var doorIsOpen : boolean = false;
private var doorTimer : float = 0.0;
private var doorOpenTime : float = 3.0;
private var currentDoor : GameObject;

var doorOpenSound : AudioClip;
var doorShutSound : AudioClip;
   /*   Script para o funcionamento da porta da cabana   */
function Update () {
	if (doorIsOpen) {
		doorTimer += Time.deltaTime;
	}
	if (doorTimer > doorOpenTime) {
		Door (doorShutSound, false, "doorshut", currentDoor);
		doorTimer = 0.0;
	}
   /*   A porta se mantém aberta por um determinado tempo   */
	var hit : RaycastHit;
   /*   Função Raycast para 'transportar' um evento de lugar, no caso, o detector de colisão da porta   */
	if (Physics.Raycast(transform.position, transform.forward, hit, 5.0)){
		if (hit.collider.gameObject.tag == "outpostDoor" && doorIsOpen == false && BatteryCollect.charge >= 4){
			currentDoor = hit.collider.gameObject;
			Door (doorOpenSound, true, "dooropen", currentDoor);
			GameObject.Find("Battery GUI").GetComponent(GUITexture).enabled = false;
		}
		else if(hit.collider.gameObject.tag == "outpostDoor" && doorIsOpen == false && BatteryCollect.charge < 4)
		{
			GameObject.Find("Battery GUI").GetComponent(GUITexture).enabled = true;
			TxHints.message = "VOCE PRECISA DE 4 PILHAS PARA ABRIR A PORTA";
			TxHints.textOn = true;
		}
	}
}








function OnControllerColliderHit (hit: ControllerColliderHit){

	var crosshairObj : GameObject = GameObject.Find("Crosshair");
	var crosshair : GUITexture = crosshairObj.GetComponent(GUITexture);

	if (hit.collider == GameObject.Find("mat").collider ){
		CoconutThrow.canThrow = true;
		crosshair.enabled = true;
		TxHints.textOn = true;
		TxHints.message =   "Derruba porra";
		GameObject.Find("Tx Hint GUI").transform.position.y = 0.2;
	}
	else{
		CoconutThrow.canThrow = false;
		crosshair.enabled = false;
		GameObject.Find("Tx Hint GUI").transform.position.y = 0.5;
	}

}


function Door (aClip : AudioClip, openCheck : boolean, animName : String, thisDoor : GameObject) {
	audio.PlayOneShot (aClip);
	doorIsOpen = openCheck;
	thisDoor.transform.parent.animation.Play (animName);
}


function OnTriggerEnter (collisionInfo : Collider){
	if (collisionInfo.gameObject.tag == "Battery")
	{
	BatteryCollect.charge ++;
	audio.PlayOneShot (batteryCollect);
	Destroy (collisionInfo.gameObject);
	}
}




@script RequireComponent (AudioSource)
