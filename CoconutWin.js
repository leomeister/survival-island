﻿static var targets : int = 0;
private var haveWon : boolean = false;
public var win : AudioClip;
public var battery : GameObject;

function Update () {
	if(targets == 3 && haveWon == false){
		targets = 0;
		audio.PlayOneShot(win);
		/*   só é possível ganhar uma vez no minigame, e na ocasião da vitória, uma bateria é instanciada, permitindo o   progresso do jogador   */
		Instantiate(battery, Vector3(transform.position.x, transform.position.y+2, transform.position.z),transform.rotation);
		haveWon = true;
	}
}

@script RequireComponent (AudioSource)
