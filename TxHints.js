﻿static var textOn : boolean = false;
static var message : String;
private var timer : float = 0.0;

function Start () {
timer = 0.0;
textOn = false;
guiText.text = "";

}
   /*   este script possibilita a utilização de dicas em texto que se mantém por 3 segundos na tela   */
function Update () {
if (textOn)
	{
	guiText.enabled = true;
	guiText.text = message;
	timer += Time.deltaTime;
	}
	if (timer >= 3.0)
	{
		textOn = false;
		guiText.enabled = false;
		timer = 0.0;
	}

}
