﻿var targetRoot : GameObject;
private var beenHit : boolean = false;   /*   estas variáveis só devem ser acessadas pelo script CoconutCollision   */
private var timer : float = 0.0;
var hitSound : AudioClip;
var resetSound : AudioClip;

function OnCollisionEnter (theObject : Collision ){
	if(beenHit == false && theObject.gameObject.name == "coconut"){
		audio.PlayOneShot(hitSound);
		targetRoot.animation.Play("down");
		beenHit = true;
		CoconutWin.targets++;
		/*   A vitória no minigame dos cocos se configura por acertar os três alvos dentro de um limite de tempo   */
	}


}

function Update () {   /*   A função Update já é presente no Unity   */
	if(beenHit){
		timer += Time.deltaTime;
	}
	if(timer > 3){
		audio.PlayOneShot(resetSound);
		targetRoot.animation.Play("up");
		beenHit = false;
		CoconutWin.targets--;
		timer = 0.0;
	}
}

@script RequireComponent (AudioSource)
