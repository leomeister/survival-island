﻿var theTexture : Texture2D;
private var StartTime : float;
var fadeTemp : float = 30.0;
var speed : float = 0.2;

function OnLevelWasLoad(){
	StartTime = Time.time;
}

function Update () {
	if(Time.time - StartTime >= fadeTemp){
		Destroy(gameObject);
	}
}

function OnGUI (){
	GUI.color = Color.white;
	//função para dar dinamicidade às texturas com o fogo
	//interpolando em 1(completamente visivel) ate 0(completamente invisivel)
	//GUI.color = Mathf.Lerp(1.0,0.0,(Time.time - StartTime)*speed);
	GUI.DrawTexture(Rect(0,0,Screen.width, Screen.height),theTexture);
}

