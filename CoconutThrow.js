﻿static var canThrow : boolean = false;   //   canThrow indica se o minigame foi ativado
var throwSound : AudioClip;
var coconutObject : Rigidbody;           //   coconutobject é uma classe filha da classe Rigidbody
var throwForce : float = 25;		 //   variável para a velocidade de saída do objeto (coco)


function Update () {
	if(Input.GetButtonUp("Fire1") && canThrow ){
		audio.PlayOneShot(throwSound);   //   Toca o som selecionado para o lançamento
		var newCoconut = Instantiate(coconutObject, transform.position, transform.rotation);   //   é instânciado o objeto
		newCoconut.name =  "coconut";
		newCoconut.rigidbody.velocity = transform.TransformDirection(Vector3 (0,0,throwForce));   /*   aqui são utilizados métodos pertencentes à classe rigidbody, incluídas no Unity   */
		Physics.IgnoreCollision(transform.root.collider, newCoconut.collider, true); /*   sem esta linha, os cocos instanciados se colidem com o próprio jogador   */
	}
}

@script RequireComponent(AudioSource);
